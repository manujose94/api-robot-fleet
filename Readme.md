# API Robot Fleet

API to interact with RMF fleet and an example of api that interacts with an example of free fleet.

This api has been developed to work with RMF and then a direct interaction with the RMF API contained in RMF Demos is used as an example. 

Here are the following projects being tested:

[API_RMF-Middleware](./API_RMF/)

[Two-Fleets-in-RMF_Demos](./API_RMF/docs/TWO-FLEETS.md)