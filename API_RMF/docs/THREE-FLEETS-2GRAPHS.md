# THREE FLEETS

New mod. version of rmf_demos: [rmf_demos_office_3fleets_2graph](./versions_rmf_demos/rmf_demos_office_3fleets_2graph)

**Files changed**:

~/rmf_ws/rmf_ws/src/demonstrations/rmf_demos/rmf_demos/launch/office.launch.xml

~/rmf_ws/src/demonstrations/rmf_demos/rmf_demos_maps/office/office.building.yaml (via Traffic Editor)

~/rmf_ws/src/demonstrations/rmf_demos/rmf_demos_dashboard_resources/office$ -> dashboard_config.json


~/rmf_ws/rmf_ws/install/rmf_demos/share/rmf_demos/include/adapters/cleanerBotE_adapter.launch.xml

Change:
```xml
<!--  Whether it can perform loop  -->
<arg name="perform_loop" value="false"/>
```
to:
```xml
<!--  Whether it can perform loop  -->
<arg name="perform_loop" value="true"/>
```
**Added new Files**:

~/rmf_ws/src/demonstrations/rmf_demos/rmf_demos_tasks/rmf_demos_tasks$ ->  office_docker_config.yaml

```yaml
cleanerBotE:
  zone_1:
    level_name: "L1"
    path: [ [21.75, -4.209, 0.0],
            [22.75, -3.336 ,1.57],
            [22.9, -4.51, 0.0],
            [22.54, -5.407, 1.57] ]
    finish_waypoint: "zone_1"
```

this is used to use the following task:

```
cluster1@cluster1:~$ cd rmf_ws/
cluster1@cluster1:~/rmf_ws$ source ~/rmf_ws/install/setup.bash
cluster1@cluster1:~/rmf_ws$ ros2 run rmf_demos_tasks dispatch_clean -cs zone_1 -st 0 --use_sim_time
[INFO] [1637652192.662999385] [task_requester]: Using Sim Time

Generated clean request: 
 rmf_task_msgs.srv.SubmitTask_Request(requester='', description=rmf_task_msgs.msg.TaskDescription(start_time=builtin_interfaces.msg.Time(sec=71, nanosec=38000000), priority=rmf_task_msgs.msg.Priority(value=0), task_type=rmf_task_msgs.msg.TaskType(type=4), station=rmf_task_msgs.msg.Station(task_id='', robot_type='', place_name=''), loop=rmf_task_msgs.msg.Loop(task_id='', robot_type='', num_loops=0, start_name='', finish_name=''), delivery=rmf_task_msgs.msg.Delivery(task_id='', items=[], pickup_place_name='', pickup_dispenser='', pickup_behavior=rmf_task_msgs.msg.Behavior(name='', parameters=[]), dropoff_place_name='', dropoff_ingestor='', dropoff_behavior=rmf_task_msgs.msg.Behavior(name='', parameters=[])), clean=rmf_task_msgs.msg.Clean(start_waypoint='zone_1')))

[INFO] [1637652192.916002052] [task_requester]: Submitting Clean Request
[INFO] [1637652192.917268235] [task_requester]: Request was successfully submitted and assigned task_id: [Clean0]
cluster1@cluster1:~/rmf_ws$ ros2 run rmf_demos_tasks dispatch_clean -h
usage: dispatch_clean [-h] -cs CLEAN_START [-st START_TIME] [-pt PRIORITY] [--use_sim_time]

optional arguments:
  -h, --help            show this help message and exit
  -cs CLEAN_START, --clean_start CLEAN_START
                        Clean start waypoint
  -st START_TIME, --start_time START_TIME
                        Start time from now in secs, default: 0
  -pt PRIORITY, --priority PRIORITY
                        Priority value for this request
  --use_sim_time        Use sim time, default: false
```



