# TWO FLEETS



**Files changed**:

~/rmf_ws/rmf_ws/src/demonstrations/rmf_demos/rmf_demos/launch/office.launch.xml
~/rmf_ws/src/demonstrations/rmf_demos/rmf_demos_dashboard_resource/office/main.js
~/rmf_ws/src/demonstrations/rmf_demos/rmf_demos_maps/office/office.building.yaml

**Add new file**:

~/rmf_ws/src/demonstrations/rmf_demos/rmf_demos/launch/include/adapters/robtonikFleet_adapter.launch.xml

**New fleet**:

- Name of fleet: robotnikFleet
- Name of fleet robots: robotnikRobot1, robotnikRobo2



Open building map in **traffic_editor**

<img src="./imgs/2fleets_0_traffic_editor_mapDemoOffice.png" style="zoom: 77%;" />

Add new spawns for new robots

<img src="./imgs/2fleets_2_traffic_editor_addNewSpawn.png" style="zoom:67%;" />

Rebuild rmf_demos and check simulation:
```
source ~/rmf_ws/install/setup.bash
ros2 launch rmf_demos office.launch.xml
```
**Rviz map**

![](./imgs/2fleets_3_rviz.png)

**Gazebo Simulation**

![](./imgs/2fleets_4_gazebo.png)

**Panel Dashboard of RMF_DEMOS**

![](./imgs/2fleets_5_rmf_panel.png)

**Check Fleet States via ROS2**

![](./imgs/2fleets_6_fleet_states.png)