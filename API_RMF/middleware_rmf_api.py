#!/usr/bin/env python3

import sys
import os
import argparse
import time
import json
import logging
from threading import Thread
import requests as req
from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_socketio import SocketIO, emit, disconnect



app = Flask(__name__)
cors = CORS(app, origins=r"/*")

socketio = SocketIO(app, async_mode='threading')
socketio.init_app(app, cors_allowed_origins="*")

#IP and Port Web Server RMF DEMOS
SERVER_URL_RMF="0.0.0.0:8080"



# logging config
logging.getLogger('werkzeug').setLevel(logging.ERROR)  # hide logs from flask
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s',
                    filename='api_rmf_server.log',
                    filemode='w')

###############################################################################


@app.route('/api/submit_task', methods=['POST'])
def submit():
    print("request", request.json)
    """REST Call to submit task"""
    #Replace for http post
    #task_id, err_msg = dispatcher_client.submit_task_request(request.json)
    """logging.debug(f" ROS Time: {dispatcher_client.ros_time()} | \
        Task Submission: {json.dumps(request.json)}, error: {err_msg}")
    return jsonify({"task_id": task_id, "error_msg": err_msg})"""
    try:
        post_url=SERVER_URL_RMF+'/submit_task'
        headers = {"Content-Type": "application/json; charset=utf-8"}
        json_body = request.json
        response = req.post(post_url, headers=headers,json=json_body) # OK -> <Response [200]>
        
        if response.status_code == 200:
            print(response.json()["error_msg"])
            return jsonify({"succes":len(response.json()["error_msg"]) == 0,"message": "submit task sending", "response":response.json()})
        else:
            return jsonify({"succes":False,"message": "submit task failed to send", "response":response.text})
    except req.exceptions.RequestException as e: #handle all the errors here
        return jsonify({"success": False, "error":"Error to post: "+post_url,"timeout":"3s"})
    
  

@app.route('/api/cancel_task', methods=['POST'])
def cancel():
    #cancel_id = req.json['task_id']
    #Replace for http post
    try:
        print("request", request.json)
        post_url=SERVER_URL_RMF+'/cancel_task'
        headers = {"Content-Type": "application/json; charset=utf-8"}
        json_body = request.json
        response = req.post(post_url, headers=headers,json=json_body) # OK -> <Response [200]>
        
        if response.status_code == 200:
            print(response.json())
            return jsonify({"succes":response.json()["success"],"message": "cancel task sending", "response":response.json()})
        else:
            return jsonify({"succes":False,"message": "cancel task failed to send", "response":response.text})
    except req.exceptions.RequestException as e: #handle all the errors here
        return jsonify({"success": False, "error":"Error to post: "+post_url,"timeout":"3s"})

@app.route('/api/task_list', methods=['GET'])
def status():
    try:
        get_url=SERVER_URL_RMF+'/task_list'
        r = req.get(get_url,timeout=3)
    except req.exceptions.RequestException as e: #handle all the errors here
        return jsonify({"success": "false", "error":"Error to get: "+get_url,"timeout":"3s"})
        
    
    #Replace for http GET (Return data of 0.0.0.0:8080  server not dispachet)
    #task_status = jsonify(dispatcher_client.get_task_status())
    logging.debug(" API Time: %s | \
        Task Status: %s",time.strftime("%Y-%b-%d %H:%M:%S"),json.dumps(r.text))
    return jsonify(r.json())


@app.route('/api/robot_list', methods=['GET'])
def robots():
    try:
        get_url=SERVER_URL_RMF+'/robot_list'
        r = req.get(get_url,timeout=3)
    except req.exceptions.RequestException as e: #handle all the errors here
        return jsonify({"success": "false", "error":"Error to get: "+get_url, "timeout":"3s"})
    #Replace for http GET
   # robot_status = jsonify(dispatcher_client.get_robot_states())
    logging.debug(" API Time: %s | \
        Task Status: %s",time.strftime("%Y-%b-%d %H:%M:%S"),json.dumps(r.text))
    return jsonify(r.json())


@app.route('/building_map', methods=['GET'])
def building_map():
    #Replace for http GET
    #building_map_data = jsonify(dispatcher_client.get_building_map_data())
    """logging.debug(f" ROS Time: {dispatcher_client.ros_time()} | \
        building_map_data: {building_map_data}")
    return building_map_data"""
    return jsonify({"message": "building_map not available"})

###############################################################################

def broadcast_states():
    """
    Robot_states, tasks_status, and ros_time are being broadcasted
    to frontend UIs via socketIO, periodically (every 2s)
    """
    ns = '/status_updates'
    """     
    while rclpy.ok():
        with app.test_request_context():
            #tasks = dispatcher_client.get_task_status()
            #robots = dispatcher_client.get_robot_states()
            #ros_time = dispatcher_client.ros_time()
            socketio.emit('task_status', tasks, broadcast=True, namespace=ns)
            socketio.emit('robot_states', robots, broadcast=True, namespace=ns)
            socketio.emit('ros_time', ros_time, broadcast=True, namespace=ns)
            logging.debug(f" ROS Time: {ros_time} | "
                            "active tasks: "
                            f"{len(dispatcher_client.active_tasks_cache)}"
                            " | terminated tasks: "
                            f"{len(dispatcher_client.terminated_tasks_cache)}"
                            f" | active robots: {len(robots)}")
            time.sleep(2) 
    """
        

###############################################################################


def main(args=None):
    mid_server_ip = "0.0.0.0"
    mid_port_num = 8088

    if "SERVER_IP_ADDRESS" in os.environ:
        mid_server_ip = os.environ['SERVER_IP_ADDRESS']

    if "SERVER_PORT" in os.environ:
        mid_port_num = os.environ['SERVER_PORT']

    if "SERVER_URL_RMF" in os.environ:
        global SERVER_URL_RMF
        SERVER_URL_RMF = os.environ['SERVER_URL_RMF']
        if not SERVER_URL_RMF.startswith( 'http://' ):
            SERVER_URL_RMF = 'http://'+SERVER_URL_RMF
        print(f"Set Server URL RMF to: {SERVER_URL_RMF} for source data")

    print(f"Set MID API Server IP to: {mid_server_ip}:{mid_port_num}")


    print("Starting Middleware API Server")
    app.run(host=mid_server_ip, port=mid_port_num, debug=True)
    


if __name__ == "__main__":
    main(sys.argv)