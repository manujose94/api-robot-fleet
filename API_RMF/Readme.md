# API RMF_CORE
The objective of this project is to create an api to interact with RMF.
---
**Options**
A- Made Middleware that attacks the rmf server_api rmf of rmfdemos
B- Create new ROS2 package in rmf_demos containing:
- B.1: rmf_api that directly attacks the dispatcher.
- B.2: new Dispatcher, with new Node name
- B.3 Try to attack the already launched dispatcher (tedious)

Note: RMF contains a RMF_demos project with an API with the following interacction:
http REQUESTS-> (api_server.py) -> function calls -> (DispatcherClient) -> RMF nodes.

### RMF_API

This is a package, then must be here:

Once it installed:
/home/cluster1/rmf_ws/install/rmf_demos_panel/lib/python3.8/site-packages/rmf_api/api_server.py

Original RMF_DEMOS packege installed:
/home/cluster1/rmf_ws/install/rmf_demos_panel/lib/python3.8/site-packages/rmf_demos_panel/api_server.py

#### Current changes RMF_DEMOS

```
~/rmf_ws/src/demonstrations/rmf_demos/rmf_demos/
--launch/office.launch.xml
--launch/include/adapters/robotnikFleet_adapter.launch.xml
--rmf_demos_dashboard_resources/office/main.js (not necessary)
```
Another changed file:

```
~/rmf_ws/src/demonstrations/rmf_demos/rmf_demos_maps/maps/office/office.building.yaml
```
This file is generated via package traffic-editor


## How it works

### API 

Test Run with office world

Start Office World
ros2 launch rmf_demos_gz office.launch.xml

#### Simple CURL Test

```bash
# Submit Task (POST)
curl -X POST http://localhost:8088/api/submit_task \
-d '{"task_type":"Loop", "start_time":0, "description": {"start_name": "coe", "finish_name": "pantry", "num_loops":1}}' \
-H "Content-Type: application/json" 

# Get Task List (GET)
curl http://localhost:8088/api/task_list

# Cluster1 - Get Task List (GET)
curl http://158.42.163.57:8080/task_list
[{"description":"cleanerBotE_0_Charger --> zone_1_start x1","done":false,"end_time":102,"fleet_name":"cleanerBotE","priority":2,"progress":"Delayed","robot_name":"cleanerBotE_0","start_time":53,"state":"Active/Executing","submited_start_time":52,"task_id":"Loop1","task_type":"Loop"},{"description":"robotnikRobot1_charger --> lounge x1","done":true,"end_time":84,"fleet_name":"robotnikFleet","priority":0,"progress":"100%","robot_name":"robotnikRobot1","start_time":50,"state":"Completed","submited_start_time":50,"task_id":"Loop0","task_type":"Loop"},{"description":"lounge --> supplies x1","done":true,"end_time":230,"fleet_name":"robotnikFleet","priority":0,"progress":"100%","robot_name":"robotnikRobot1","start_time":191,"state":"Completed","submited_start_time":191,"task_id":"Loop2","task_type":"Loop"}]

# Cluster1 - Cancel Task 
curl --location --request POST 'http://158.42.163.57:8080/cancel_task' \
--header 'Content-Type: application/json' \
--data-raw '{
    "task_id": "Loop1"
}'
```

#### rmf-panel-js
 
A front end dashboard, [rmf-panel-js](https://github.com/open-rmf/rmf-panel-js) is also provided.
This will will be the client for the rmf_demos's api server.

Launch the a web dashboard: https://open-rmf.github.io/rmf-panel-js/

---

### API Endpoints

Endpoints | Type | Parameters | Description
--- | --- | --- | ---
/submit_task | POST | task description json | This supports submission of task. This response with an assigned task_id is the task is accepted
/cancel_task | POST | task_id string | Cancel an existing task in rmf.
/task_list | GET | NA | Get list of task status in RMF (include active and terminated tasks)
/robot_list | GET | NA | Get list of Robot states in RMF
/task_status | SocketIO | NA | Constant broadcast of task list
/robot_states | SocketIO | NA | Constant broadcast of robot list

#### Examples 

http://localhost:8088/api/task_list
´´´json

    {
        "description": "cleanerBotE_0_Charger --> zone_1_start x1",
        "done": false,
        "end_time": 102,
        "fleet_name": "cleanerBotE",
        "priority": 2,
        "progress": "Delayed",
        "robot_name": "cleanerBotE_0",
        "start_time": 53,
        "state": "Active/Executing",
        "submited_start_time": 52,
        "task_id": "Loop1",
        "task_type": "Loop"
    },
    {
        "description": "robotnikRobot1_charger --> lounge x1",
        "done": true,
        "end_time": 84,
        "fleet_name": "robotnikFleet",
        "priority": 0,
        "progress": "100%",
        "robot_name": "robotnikRobot1",
        "start_time": 50,
        "state": "Completed",
        "submited_start_time": 50,
        "task_id": "Loop0",
        "task_type": "Loop"
    },
    {
        "description": "lounge --> supplies x1",
        "done": true,
        "end_time": 230,
        "fleet_name": "robotnikFleet",
        "priority": 0,
        "progress": "100%",
        "robot_name": "robotnikRobot1",
        "start_time": 191,
        "state": "Completed",
        "submited_start_time": 191,
        "task_id": "Loop2",
        "task_type": "Loop"
    }
]
´´´